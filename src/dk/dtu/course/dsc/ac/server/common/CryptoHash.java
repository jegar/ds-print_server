package dk.dtu.course.dsc.ac.server.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
//import java.io.RandomAccessFile;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Arrays;

public class CryptoHash {

	private final int saltLenght = 12;
	private SecureRandom randGen;
	private MessageDigest hasher;
	private Signature signature;
	private int signatureLenght = 256;

	public CryptoHash(String algorithm) throws NoSuchAlgorithmException {
		randGen = SecureRandom.getInstanceStrong();
		hasher = MessageDigest.getInstance(algorithm);
		signature = Signature.getInstance("SHA256withRSA");
	}

	public String createFileHash(String filename) {
		String result = null;
		hasher.reset();
		try {
			InputStream fin = new FileInputStream(filename);
			InputStream in = new BufferedInputStream(fin);

			int numRead;
			byte[] buffer = new byte[1024];
			while ((numRead = in.read(buffer)) > -1) {
				hasher.update(buffer, 0, numRead);
			}
			hasher.digest();
			result = byteToHex(buffer);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public byte[] createHash(String password) throws NoSuchAlgorithmException, IOException {

		byte[] salt = generateSalt(this.saltLenght);

		byte[] encrPass = doHash(salt, password.getBytes());
		byte[] buffer = new byte[encrPass.length + salt.length];
		System.arraycopy(salt, 0, buffer, 0, salt.length);
		System.arraycopy(encrPass, 0, buffer, salt.length, encrPass.length);
		return buffer;
	}

	private byte[] doHash(byte[] salt, byte[] psw) {

		hasher.reset();
		hasher.update(psw);
		hasher.update(salt);
		return hasher.digest();
	}

	public boolean compareHash(String password, byte[] hashedPassword) {
		byte[] salt = Arrays.copyOfRange(hashedPassword, 0, saltLenght);
		byte[] pass = Arrays.copyOfRange(hashedPassword, saltLenght, hashedPassword.length);
		boolean result = true;

		byte[] passToTest = doHash(salt, password.getBytes());

		for (int i = 0; i < pass.length; i++) {
			if (passToTest[i] != pass[i]) {
				result = false;
				break;
			}
		}
		return result;
	}

	public void signFile(String filename, String filedest, PrivateKey priv)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, IOException {

		signature.initSign(priv);
		InputStream fin = new FileInputStream(filename);
		InputStream in = new BufferedInputStream(fin);

		int numRead;
		byte[] buffer = new byte[1024];
		while ((numRead = in.read(buffer)) > -1) {
			signature.update(buffer, 0, numRead);
		}
		in.close();

		byte[] generatedSignature = signature.sign();

		OutputStream fou = new FileOutputStream(filedest);
		OutputStream out = new BufferedOutputStream(fou);
		out.write(generatedSignature);
		out.close();
	}

	public boolean checkFileSignature(String filename, String filesign, PublicKey pub)
			throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, IOException {

		signature.initVerify(pub);
		InputStream fin = new FileInputStream(filename);
		InputStream in = new BufferedInputStream(fin);

		int numRead;
		byte[] buffer = new byte[1024];
		while ((numRead = in.read(buffer)) > -1) {
			signature.update(buffer, 0, numRead);
		}
		in.close();

		fin = new FileInputStream(filesign);
		in = new BufferedInputStream(fin);
		byte[] sigByte = new byte[this.signatureLenght];
		in.read(sigByte);
		in.close();
		//
		// RandomAccessFile ranf = new RandomAccessFile(filename, "r");
		// long dataBytes = ranf.length() - signatureLenght;
		// byte[] buffer = new byte[1024];

		// int currentRead = (int)Math.min(dataBytes, 1024);
		// while (currentRead > 0) {
		// ranf.readFully(buffer, 0, currentRead);
		// dataBytes -= currentRead;
		// currentRead = (int)Math.min(dataBytes, 1024);
		// signature.update(buffer);
		// }
		// byte[] sigBuff = new byte[signatureLenght];
		// ranf.readFully(sigBuff, 0, signatureLenght);
		// ranf.close();
		return signature.verify(sigByte);
	}

	private String byteToHex(byte[] input) {
		String result = "";

		for (byte a : input) {
			result += Integer.toString((a & 0xFF) + 0x100, 16).substring(1);
		}
		return result;
	}

	private byte[] generateSalt(int lenght) {
		byte[] salt = new byte[lenght];
		this.randGen.nextBytes(salt);
		return salt;
	}

	public long getRandLong() {
		return this.randGen.nextLong();
	}

	public int getSaltLenght() {
		return saltLenght;
	}

	public int getSignLenght() {
		return signatureLenght;
	}
}
