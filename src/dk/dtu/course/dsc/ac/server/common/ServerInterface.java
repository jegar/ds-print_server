package dk.dtu.course.dsc.ac.server.common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map.Entry;

public interface ServerInterface extends Remote {

	/**
	 * Logs the user in the system, returns a ticket that the client must send in
	 * its next request.
	 * 
	 * @param username
	 *            Name or nick of the registered user.
	 * @param password
	 *            Password of the user.
	 * @return Ticket that the client must give back, or null if the user couldn't
	 *         be logged in.
	 * @throws RemoteException
	 */
	public byte[] login(String username, String password) throws RemoteException;

	/**
	 * Prints a file in a specified printer. It actually adds the element to the
	 * queue of that particular printer. In this implementation, the printer is just
	 * a file on which the filename will be printed.
	 * 
	 * Note: In the current implementation, a printer takes 10s to print a file.
	 * 
	 * @param ticket
	 *            The received ticket from a previous (including the login)
	 *            operation.
	 * @param filename
	 *            The name of the file that must be printed by the printer.
	 * @param printer
	 *            The printer on which the file will be printed.
	 * @return Ticket that the client must give back, null if the previous ticket
	 *         couldn't be validated or if the filename is not valid.
	 * @throws RemoteException
	 */
	public byte[] print(byte[] ticket, String filename, String printer) throws RemoteException;

	/**
	 * Shows the print queue on the user's display in the format <job number> <file
	 * name>
	 * 
	 * Note: In the current implementation, a printer takes 10s to print a file.
	 * 
	 * @param ticket
	 *            The received ticket from a previous (including the login)
	 *            operation.
	 * @return Ticket that the client must give back, null if the previous ticket
	 *         couldn't be validated
	 * @throws RemoteException
	 */
	public Entry<byte[], List<Entry<Integer, String>>> queue(byte[] ticket) throws RemoteException;

	/**
	 * Moves a specified job to the top of the queue on that particular printer.
	 * 
	 * Note: In the current implementation, a printer takes 10s to print a file.
	 * 
	 * @param ticket
	 *            The received ticket from a previous (including the login)
	 *            operation.
	 * @param job
	 *            The job id.
	 * @return Ticket that the client must give back, null if the previous ticket
	 *         couldn't be validated
	 * @throws RemoteException
	 */
	public byte[] topQueue(byte[] ticket, int job) throws RemoteException;

	/**
	 * Starts the printer server, it also starts the log file.
	 * 
	 * @param ticket
	 *            The received ticket from a previous (including the login)
	 *            operation.
	 * @return Ticket that the client must give back, null if the previous ticket
	 *         couldn't be validated
	 * @throws RemoteException
	 */
	public byte[] start(byte[] ticket) throws RemoteException;

	/**
	 * Stops the printer server.
	 * 
	 * @param ticket
	 *            The received ticket from a previous (including the login)
	 *            operation.
	 * @return Ticket that the client must give back, null if the previous ticket
	 *         couldn't be validated
	 * @throws RemoteException
	 */
	public byte[] stop(byte[] ticket) throws RemoteException;

	/**
	 * Stops the print server, clears the print queue and starts the print server
	 * again
	 * 
	 * @param ticket
	 *            The received ticket from a previous (including the login)
	 *            operation.
	 * @return Ticket that the client must give back, null if the previous ticket
	 *         couldn't be validated
	 * @throws RemoteException
	 */
	public byte[] restart(byte[] ticket) throws RemoteException;

	/**
	 * Prints status of printers on the user's display.
	 * 
	 * @param ticket
	 *            The received ticket from a previous (including the login)
	 *            operation.
	 * @return Ticket that the client must give back, null if the previous ticket
	 *         couldn't be validated and aa list of the status of the different
	 *         printers.
	 * @throws RemoteException
	 */
	public Entry<byte[], List<Entry<String, String>>> status(byte[] ticket) throws RemoteException;

	/**
	 * Prints the value of the parameter on the user's display.
	 * 
	 * @param ticket
	 *            The received ticket from a previous (including the login)
	 *            operation.
	 * @param parameter
	 *            Name of the parameter to check.
	 * @return Ticket that the client must give back, null if the previous ticket
	 *         couldn't be validated
	 * @throws RemoteException
	 */
	public Entry<byte[], String> readConfig(byte[] ticket, String parameter) throws RemoteException;

	/**
	 * Sets a parameter to a value.
	 * 
	 * @param ticket
	 *            The received ticket from a previous (including the login)
	 *            operation.
	 * @param parameter
	 *            Name of the parameter to check.
	 * @param value
	 *            Value of the parameter.
	 * @return Ticket that the client must give back, null if the previous ticket
	 *         couldn't be validated
	 * @throws RemoteException
	 */
	public byte[] setConfig(byte[] ticket, String parameter, String value) throws RemoteException;
}
